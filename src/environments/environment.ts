// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCVOxn6zlDnLlQ_53F5RhRN4RFsVrzrbck",
    authDomain: "homework4-48c38.firebaseapp.com",
    databaseURL: "https://homework4-48c38.firebaseio.com",
    projectId: "homework4-48c38",
    storageBucket: "homework4-48c38.appspot.com",
    messagingSenderId: "643031737141"
  
}
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
